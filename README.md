# Individual Repository for DevOps

This repository contains the files for all the class assignemts of DevOps.

* [Class Assignment 1](https://bitbucket.org/1191747/devops-19-20-b-1191747/src/master/ca1/)

* [Class Assignment 2 - parte 1](https://bitbucket.org/1191747/devops-19-20-b-1191747/src/master/ca2/README.md)

* [Class Assignment 2 - parte 2](https://bitbucket.org/1191747/devops-19-20-b-1191747/src/master/ca2/ca2_part2/README.md)

* [Class Assignment 3 - parte 1](https://bitbucket.org/1191747/devops-19-20-b-1191747/src/master/ca3/ca3-parte1/)

* [Class Assignment 3 - parte 2](https://bitbucket.org/1191747/devops-19-20-b-1191747/src/master/ca3/ca3-parte2/)

* [Class Assignment 4](https://bitbucket.org/1191747/devops-19-20-b-1191747/src/master/ca4/README.md)

* [Class Assignment 5 - part1](https://bitbucket.org/1191747/devops-19-20-b-1191747/src/master/ca5/ca5_part1/)

* [Class Assignment 5 - part2](https://bitbucket.org/1191747/devops-19-20-b-1191747/src/master/ca5/ca5_part2/)

* [Class Assignment 6](https://bitbucket.org/atb/devops-19-20-rep-template/src/master/ca6/)

**Note**
You should use Markdown Syntax so that the contents are properly rendered in Bitbucket. See how to write README files for Bitbucket in [here](https://confluence.atlassian.com/bitbucket/readme-content-221449772.html)
