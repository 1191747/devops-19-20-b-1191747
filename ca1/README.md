# Class Assignment 1 Report

This assignment is about a centralized Version Control Systems (VCB) called Git. This system is used for tracking changes in source code during software development.

The source code for this assignment is located in the folder [ca1/tut-basic]((https://1191747@bitbucket.org/1191747/devops-19-20-b-1191747.git)

## 1. Analysis, Design and Implementation

Create a repository in BitBucket

Download the project

### At the comand line

Change diretory to the repository: $ cd C:\Users\AnaRuteAzevedo\Documents\ISEP\DevopsProject\devops-19-20-b-1191747

Check git configuration:$ git config -l

Set may user name and email Adress: $git config --global user.name "Ana Azevedo"; §git config --global user.email"1191747@isep.ipp.pt"

Start controlling the project: $ git init

Create the inicial tag version as v1.2.0 and shut the issue created at bitbucket: $ git tag -a v1.2.0 - m "FIX#1 Tag the initial version as v1.2.0"

Send the tag to the repository: $git push origin v1.2.0

If you want to see all tags created: $ git tag

Create a branch called email-field: $git branch email-field

Send branch to the repository: $git push --set-upstream origin email-field

Change to the new branch: $git checkout email-field

### At IntelliJ:

Update Java

Create the support for email field at Class Employee (basic-src-main-java-Employeee)

Create a Class EmployeeTest to validat its atributes (basic-src-test-java-EmployeeTest)

Update JavaScript

Create the support for email field at app.js (basic-main-js-app.js)

Run the basic-src-java-DatabaseLoader

### At the comand line

Go to master branch: $git checkout master

Merge branch: $ git merge email-field

Update repository: $ git push --set-upstream origin email-field

Commit all the changes and shut bitbucket issue: $ git commint -a -m "Fix#4 - Employee - teste to validat attributes and debug the server and client"

Update repositort: $git push

Change to basic project: $cd: C:\Users\AnaRuteAzevedo\Documents\ISEP\DevopsProject\devops-19-20-b-1191747\ca1\tut-react-and-spring-data-rest\basic

Run the project: mvnm spring-boot run

Open the site to validate it is working: www.http://localhost:8080/

Create the new tag version as v1.3.0 and shut the issue created at bitbucket: $ git tag -a v1.3.0 - m "FIX#5-Create Tag v1.3.0"

Send the tag to the repository: $git push origin v1.3.0

Create a branch called fix-invalid-email: $git branch fix-invalid-email

Send branch to the repository: $git push --set-upstream origin fix-invalid-email

Change to the new branch: $git checkout fix-invalid-email

### At IntelliJ:

Validate employee email(basic-src-main-java-Employee)

Create testes to validate emails (basic-src-test-java-EmployeeTest)

### At the command line

Go to master branch: $git checkout master

Merge branch: $ git merge fix-invalid-email

Update repository: $ git push

Commit all changes: $git commit -a -m "Fix#6 - Employees - method and test - valid email"

Update repository: $ git push

Create the new tag version as v1.3.1 and shut the issue created at bitbucket: $ git tag -a v1.3.1 - m "FIX#7-Create Tag v1.3.1"

Send the tag to the repository: $git push origin v1.3.1

Create the last tag version as cal and shut the issue created at bitbucket: $ git tag -a cal - m "FIX#8-last Tag for CA1 project"

Send the tag to the repository: $git push origin cal


## 2. Analise de uma Alternativa

A alternativa, que escolhi foi Mercurial,que foi criada e desenvolvida por Matt Mackall.

Comparando o Git com o Mercurial:

 -Ambos as aplicações são de acesso gratuito e lançadas em 2005, apesar da popularidade do Git ser significativamente maior.
 
 -Ambas são programas de linha de comando, que pode ser usada em windows e IOS.
 
 -São ferramentas parecidas, com funcionamentos e comandos semelhates.

 -O GIT é mais complexo, com comandos com várias opções, o que requer um maior estudo para ser usada com segurança e eficácia.

 -Em termos de segurança as duas ferramentas são semelhantes, pois ambas podem comprometer o desenvolvimento do código, caso o usuário se engane.
 
 -As ramificações no Mercurial não tem o mesmo significado que no Git, pois referem-se a uma linha linear de conjuntos de alterações consecutivas.
  As ramificações são incorporadas nos commits e arquivadas, não podendo ser alteradas.
  
 -No Mercurial não existe área de "staging", sendo assim menos flexivel.
 
 -Apenas no GIT é permitido criar, alterar e escolher quais as ramificações a usar no commits.
 
## 3. Implementação de alternativa
 
 Criei um repositório novo no HelixTeamHub, em substituição do Bitbuchet.
 
 Dei acesso ao Professor: https://helixteamhub.cloud/black-tire-2359/projects/ca1_alt/repositories/devops-19-20-B-1191747/tree/default?path=ca1Mercurial
  
 Realizei o clone do projeto, já desenvolvido com o GIT.
 
 Criei no Mercurial, a issue 1 "First commit with project files".
 
 Na linha de comando:
 
 $hg add.
 
 $hg commint -m "First commit with project files"
 
 $hg push
 
 O projeto encontra-se assim no repositório.
