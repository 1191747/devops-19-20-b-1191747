# Parte 1 - Gradle Demo

Esta trabalho implementa um *chat room*

O *chat room* suportará vários clientes simultamente. Quando um cliente se conecta, o servidor solicita um nome. Depois, que um cliente envia um nome exclusivo, o servidor reconhece-o. Todas as mensagens desse cliente serão transmitidas para todos os outros cliente. Este *chat room* é usado para gerir o registro / saída e transmissão de mensagens de um usuário. Cada nome tem que ser exclusivo.

## 1- Pré-requisitos para implementar a aplicação

 * Java JDK 8
 * Apache Log4J 2
 * Gradle 6.6
 
## 2- Estruturar repositório e copiar projeto para o mesmo 
 
Criar no repositório a pasta ca2_part1 e ca2 - parte2

Fazer download do projecto, que se encontra em https://bitbucket.org/luisnogueira/gradle_basic_demo/src/master/

## 3- Seguir as indicações do *README*

Criar um arquivo .jar com o aplicativo

    gradlew build 

Correr no servidor

    java -cp build/libs/basic_demo-0.1.0.jar basic_demo.ChatServerApp 59001

Correr o Cliente

    gradlew runClient
    
Criar uma issue no bitbucket, realizar o commit($git commint -a -m"FIX9") e em seguinda o push ($git push)    
    
## 4- Adicionar a seguinte tarefa ao servidor

No IntelliJ, adicionar ao build.gradle:

    task runServer(type:JavaExec, dependsOn: classes){
       group = "DevOps"
       description = "Launches a chat server on localhost:59001 "
       classpath = sourceSets.main.runtimeClasspath
       main = 'basic_demo.ChatServerApp'
       args '59001'
       }
       
Para correr a nova tarefa

    gradlew runServery

Criar uma issue no bitbucket e na linha de comando realizar o commit($git commint -a -m"FIX11") e o push ($git push)      
       
## 5- Adicionar um teste unitário e atualizar o Gradle para exectutar o mesmo

Criar a pasta test: src/test/java/basic_demo

Copiar os testes unitários do emunciado

    import org.junit.Test; 
    import static org.junit.Assert.*; 
    
    public class AppTest{
    @Test public void testAppHasAGreeting() { 
    App classUnderTest = new App();
    assertNotNull("app should have a greeting", classUnderTest.getGreeting());
    } } 

Para os testes correrem com sucesso é necessário implementar a dependência do junit 4.12 no ficheiro build.gradle 

    // Use Apache Log4J for logging 
    implementation group: 'org.apache.logging.log4j', name: 'log4j-api', version: '2.11.2'
    implementation group: 'org.apache.logging.log4j', name: 'log4j-core', version: '2.11.2'
    //Unit Test dependency testCompile 'junit:junit:4.12' }
    
Correr o teste unitário e verificar se compila corretamente

Criar issue no bitbucket o commit($git commint -a -m"FIX10 e 12") e o push ($git push)

## 6- Criar uma *task Copy* para ser usada como *backup* da pasta src

No ficheiro build.gradle adicionar o seguinte

    task srcCopy(type: Copy) { 
    from 'src' 
    into 'backup'
    }
    
O conteúdo da pasta scr vai ser copiado para uma pasta denomnada backup, que se encontra localizada dentro do projeto

Realizar o seguinte comando para correr esta tarefa

    gradlew srcCopy

Criar issue no bitbucket o commit($git commint -a -m"FIX13") e o push ($git push)

## 7- Adicionar uma tarefa do tipo Zip para copiar o ficheiro da pasta src e guarda-lo num ficheiro ZIP

No ficheiro build.gradle adicionar o seguinte:

    
    task srcToZip(type: Zip) {
    from 'src' 
    archiveName 'src.zip' 
    destinationDirectory = file('srcZip')
    }

    
A pasta src vai ser zippada e guardada na pasta srcZip, localizada dentro do projeto   

Realizar o seguinte comando para correr esta tarefa

    gradlew srcToZip
    
Criar issue no bitbucket o commit($git commint -a -m"FIX#14") e o push ($git push)
    
## 8- Criar TAG ca2-part1 

Na linha de comando, criar a Tag

    git tag -a ca2-part1 -m"FIX#15"
    git push origin ca2-part1