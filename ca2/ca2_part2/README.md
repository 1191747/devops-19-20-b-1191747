# Parte 2 

Esta segunda parte tem por objetivo gerar um projeto Spring numa versão Gradle, para o basic do tutorial e tendo em atenção as dependências solicitadas.

## 1- Criar o branch tut-basic-gradle, onde serão executadas todas as tarefas:

    git branch tut-basic-gradle
    git push  --set-upstream origin tut-basic-gradle
    git checkout tut-basic-gradle

## 2 – Foram seguidos os passos indicados no CA-parte 2:

* A pasta src foi gerada vazia e será apagada. Para o seu lugar será copiada a pasta src do titorial
* Copiar para a raiz, o ficheiro webpack.conﬁg.js e package.json, para configurar a parte web do frontend do projeto
* Apagar  src/main/resources/static/built, dado que será gerado no processo de compilação
* Ao executar o gradlew bootRun (task de execução de um aplicação Spring), o servidor corre no entanto quando abrimos a página http://localhost:8080 esta encontra-se vazia, dado que o frontend ainda não está a funcionar. 
* Para dar suporte ao frontend é necessário ainda adicionar seguinte plugin no build.gradle
      
      id "org.siouan.frontend" version "1.4.1
      
* É necessário configurar este plugin no build.gradle, definindo a versão do node e do assembleScript, que vai invocar o webpack (compila o javascrit e o transforma para servir o tomcat)

         frontend {
              nodeVersion = "12.13.1"
                assembleScript = "run webpack" }    
                
* Por causa da invocação do webpack é necessário definir no ficheiro pachage.json a seguinte script

        "scripts": { 
            "watch": "webpack --watch -d",
            "webpack": "webpack"
             },
             
## 3 – Temos a aplicação a funcionar

      gradlew.build
      gradlew bootRun
      
Abrir a página http://localhost:8080 e a aplicação está a funcionar, dado que se encontram implementadas todas as configurações necessárias

## 4 – Copiar jar (java archive)

Copiar o jar que se encontra build/libs para o folder “dist”, ”, que se encontra na raiz do projeto

    clean.doFirst[
    delete 'src/main/resources/static/built/.'

     gradlew jarCopy

## 5- Apagar os ficheiros gerados pelo webpack

O *clean* é uma tarefa pré-definida no Gradle, que apaga tudo que for resultado das compilações. Esta tarefa  vai acescentar ao *clean* a função de apagar todos os ficheiros gerados pelo webpack, que se encontram na pasta src/resources/main/static/built/ , sendo também executada sempre que o Gradlew clean correr.
    
    task deteteFilesWebpack(type:Delete){
      delete src/resources/main/static/built/??
      followSymlink=true
       }  
    gradlew clean
      
## 6 – Fazer o merge do branch

     git checkout master
     git merge tut-basic-gradle
     git push --set-upstream origin tut-basic-gradle
     git commit -a -m”FIX17”
     git push

## 7 – Criar TAG ca2-part2 

Na linha de comando, criar a Tag

    git tag -a ca2-part2 -m"FIX#18 "
    git push origin ca2-part2
      

Analise de uma Alternativa
======================

* Apache Ant foi lançada em 2000 e o Gradle em 2007,  sendo  ambos sistemas de automação de compilação de código aberto, que suportam diversas sistemas operativos 
* O Apache Ant baseou-se em Make e o Gradle baseou-se no Apache Ant e no Maven
* O Apache Ant foi desenvolvido inicialmente para ser utilizada em projetos Java
* Todas as tarefas e built scripts do Apache Ant podem ser importadas para o Gradle

Apache Ant
___

* Os arquivos de construção são escritos no formato XML, sendo esta mais complexa é uma limitação para usuários menos experientes e para grandes projetos onde estes arquivos podem se tornar grandes e complexos
* Algumas "tarefas" Apache Ant mais velhas, como *javac*, *exec* e *java*, têm opções que utilizam valores padrão, que não são consistentes com as tarefas mais recentes. Ao mudar estes valores o usuário corre o risco de provocar erros em versões mais novas.
* O tratamento de exceções e de persistência de estado é limitado, por isso, a ferramenta não é considerada uma alternativa para a gestão de múltiplas projetos
* O principal benefício do Apache Ant é a sua flexibilidade. 
* A declaração de dependências é realizada de forma manual e em XML
* O Apache Ant não impõe convenções de codificação ou estruturas de projeto. Os usuários escrevam todos os comandos por si mesmos, o que às vezes leva a enormes arquivos de construção XML difíceis de manter. 

Por outro lado, Gradle:
__________________

* Utiliza uma linguagem de domínio específico (DSL) baseada em Groovy, mais simples e eficiente 
* Pode utilizar várias linguagens de programação
* A sua unidade de trabalho é a *Task*. Pode ainda realizar subclasses baseadas em tarefas de Gradle e específicar os parâmetros que quiser 
* A declaração de dependência é realizada de uma forma automática
* Os projetos tem definido a versão do Gradle, que estão a utilizar. Quando o projeto é instalado num computador sem Gradle, o Gradle Wrapper vai fazer download da versão necessária, permitindo assim que o projeto corra, não sendo assim necessário a sua instalação. 
* Para construir, testar, publicar e implantar software em qualquer plataforma, a Gradle oferece um modelo flexível que pode suportar todo o ciclo de vida do desenvolvimento, desde a compilação, pachaging e publicação de sites.

Implementação de uma Alternativa 
============================= 
* No Intellij é possível utilizar o Apache Ant como ferramenta de automação 
* À semalhança do build.gradle criar um ficheiro build.xml, que como o nome indica será em XML com o processo de construção e suas dependências em Apache Ant 
* Para alterar o projeto como um todo seria necessário escrever em XML o conteúdo do build.grade e colocar no build.xml 
* Para utilizar o Apache Ant na linha de comando é necessário instalar o mesmo e configurar na **environment variables**, no **path** adicionando o Apache Ant 
* Na linha do comando antes de cada comando é necessário colocar ”ant” 
* A título exemplificativo, realizei o ponto 13 e 14 do Ca2 parte 2 e coloquei no ficheiro build.xml.
* O build.xml está incompleto, nomeadamente em termos de dependências, pelo que as  targets não vão correr

        <target name=” jarCopy”>
    
        <copy todir=”backup/”> 
  
          <fileset dir="'build/libs/">
    
        <include name=”demo *.jar"/>
    
        <type type="file"/> 
     
        </fileset> 
     
        </copy> 
     
        </target> 
  
        ant jarCopy 
   
 
        <target name="clean"  
    
        description="clean up"> 
     
        <!-- Delete the ${build} and ${dist} directory trees --> 
      
        <delete dir="${build}"/> 
      
        <delete dir="${dist}"/> 
      
        **<delete dir=”'src/main/resources/static/built”/>**
       
        </target> 
  
        ant clean 
      
### Build.xml como um todo
      
      <project name="MyProject" default="dist" basedir=".">
      <description>
    simple example build file
     </description>
      <!-- set global properties for this build -->
      <property name="src" location="src"/>
      <property name="build" location="build"/>
     <property name="dist" location="dist"/>

     <target name="init">
    <!-- Create the time stamp -->
    <tstamp/>
    <!-- Create the build directory structure used by compile -->
    <mkdir dir="${build}"/>
    </target>

    <target name="compile" depends="init"
        description="compile the source">
    <!-- Compile the Java code from ${src} into ${build} -->
    <javac srcdir="${src}" destdir="${build}"/>
    </target>

     <target name="dist" depends="compile"
        description="generate the distribution">
    <!-- Create the distribution directory -->
    <mkdir dir="${dist}/lib"/>

    <!-- Put everything in ${build} into the MyProject-${DSTAMP}.jar file -->
    <jar jarfile="${dist}/lib/MyProject-${DSTAMP}.jar" basedir="${build}"/>
    </target>

     <target name="clean"
        description="clean up">
    <!-- Delete the ${build} and ${dist} directory trees -->
    <delete dir="${build}"/>
    <delete dir="${dist}"/>
    <delete dir=”'src/main/resources/static/built”/>
    </target>
    <target name="clean"  
    
       description="clean up"> 
     
       <!-- Delete the ${build} and ${dist} directory trees --> 
      
       <delete dir="${build}"/> 
      
       <delete dir="${dist}"/> 
      
       **<delete dir=”'src/main/resources/static/built”/>**
      
       </target>
           </project>

       



