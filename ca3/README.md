# CA3 Parte 1

## 1.Criar a máquina virtual

Para criar uma máquina virtual foi usada o VirtualBox, que é uma *hypervisior*. A minha máquina será um *host* podendo criar várias máquina virtuais como *guest*

## 1.1. Criar uma máquina virtual 

Depois de instalar o VirtualBox é necessário criar a máquina virtual:

 * New definimos: <nome da nova máquina>, o folder onde a vamos guardar a mesma, type o sistema operativo, que será linux e versão será Ubunti (64-bit)
 
 * Definimos:
    - a memória Ram pretendida (2024MB) e faz uma recomendação
    - Hard disco: Create a virtual hard disk now
 	- o tipo de disco virtual e deixamos a opção apresentada: VDI (virtualBox Desk Image)
 	- o tipo de armazenamento: Dynamically allocated
 	- o local onde vai guardar o disco vitual e tamanho
    
 * Create – temos assim a máquina virtual criada
 
 * Ao seleccionar a máquina criada, podemos ver as suas características
 
 * Start – para inicializar a máquina virtual, mas ainda não tem sistema operativo
 
## 1.2. Configurar os 2 adaptadores de rede

* as Nat – permite a máquina virtual aceder à internet

* as  Host-only Adapter (vboxnet0) – placa de rede, que permite ligar todas as máquinas virtuais ao Sistema operativo Host

* Para fazer esta instalação no menu selecionar file/host NetworkManager:

	- Na rede com o Name:vboxnet0 configure o Adapter Manually : IPV4 192.168.56.1 e IPV4 Network Mask: 255.255.255.0, verificar se está activo *Enable*
	- na máquina virtual no setting/Network para configurar as placas de rede: Adapter1 – já está configurado como Nat e o o Adapter 2 – tem que ser activo “Enabler” e selecionar o tipo de rede que quero ligar Attached: to-Host-only Adapter e á rede name:vboxnet0. Finalizar com OK
     
## 1.3. Instalar o sistema operativo Linux Ubuntu

 * Fazer dowload do Ubuntu 64 -bit PC, que não tem interface gráfica
 
 * Nos settings/storage da máquina virtual e no icon do CD rom:Controller:IDE optical drive vou seleccionar o mini.iso (ficheiro que realizei download)

* No storage da minha máquina virtual posso validar, que tenho o disco do mini.iso

* Fazendo Start da máquina virtual , esta lê o disco do Ubunto e selecciono o Install, sendo necessário definir:
 	- língua:English
 	- Nossa loalização:Portugal
	 - Local setting: english
	 - o teclado
	 -   escolher a 1 placa de rede: enp0s3
     
 - seguir o processo até ao fim
 
- configurar o utilizador e password, que vai ser administrador e utilizar da máquina

## 1.4. Ferramentas de network

* Inicializar máquina com o utilizador e login que defeni, para instalar as ferramentas de network 
	
    sudo apt install net-tools 
    
* editar o feicheiro para configurar os endereços das placas de rede:

    sudo nano/etc/netplan/01-netcfg.yaml

* Garantir que o editor de texto tem também o seguinte, para configurar a 2 placa de rede:
	Enp0s8
	Addresses:
	-192.168.56.20/24 ( se colocar 100/24 conforme sugerido, não consigo instalar o filezilla)
    
* Ctrl O Crtl X e correr o seguinte comando para aplicar as alterações: sudo netplan apply

* Com o comando *ifconfig* consido validar como estão configuradas as placas de rede

* Para mais facilmente aceder à máquina de rede e  abrir-la de terminais remotos instalar:
     sudo apt install openssh-server 
 
* Para transferir ficheiros da e para a máquina virtual este ficheiro:
	 	 sudo apt install vsftpd
		 sudo nano /etc/vsftpd.conf
		   - descomentar a linha: write_enable=YES (crtl O crtl X)
		  - sudo service vsftpd restart 
        
* Neste momento, já consigo aceder do host à máquina virtual usando o comando ssh anaazevedo@192.168.56.20

## 1.5. Instalar o FileZilla

O FireZilla permite a transferência de ficheiros para e da nossa máquina virtual

## 2. Clone

Para os projetos do CA1 e CA2 correrem é ainda necessário instalar na máqina virtual as seguintes aplicações e fazer clone do repositório:

* sudo apt install git 
* sudo apt install openjdk-8-jdk-headless
* sudo apt install maven – para poder implementar o CA1
* sudo apt install gradle  - para poder implementar o CA2
* git clone https://1191747@bitbucket.org/1191747/devops-19-20-b-1191747.git

CA1

-----

O CA1 utiliza o Maven e um servidor web como front-end. Como VM não tem placa gráfica é necessário:

      cd devops-19-20-b-1191747
     cd ca1/tut-react-and-spring-data-rest/basic
     
Para corer a aplicação:

         mvm spring-boot:run
         
No browser, da máquina host abrir a aplicação em:

         http://192.168.56.20:8080

CA2 parte1

--------

No CA2-parte1 utiliza o Gradle, no entanto como VM não tem ambiente gráfico não é possivel correr o *runClient task*

Na VM não conseguia fazer runServer no projecto ca2-part1, pelo que fiz colone do projeto original para a pasta devops-19-20-b-1191747/ca3/ca3-part1 e corri o Server
   
    Gradle runServer

Na máquina host é necessário alterar a task runClient no build.gradle, que realizei abrindo o IntelliJ
      
         task runClient(type:JavaExec, dependsOn: classes){
          group = "DevOps"
          description = "Launches a chat client that connects to a server on localhost:59001 "
          classpath = sourceSets.main.runtimeClasspath
          main = 'basic_demo.ChatClientApp' 
           __args '192.168.56.5', '59001'__
        }
Desde modo já é possível:

    gradle runClient

Abre-se a janela do Chat Room. Para obter outra janela é necessário abrir outra linha de comando voltar a fazer runClient.        

## 3. Criar pasta do Ca3 parte1

Criar no File explorer na pasta do ca3 uma subpasta com ca3-parte1 e copiar um README. md
Na linha de comando

    git pull – dado que o Readme da ca2 foi atualizado
    git cd C:\Users\AnaRuteAzevedo\Documents\ISEP\DevopsProject\devops-19-20-b-1191747\ca3 – mudar para a pasta do bitbucket do ca3
    git add C:\Users\AnaRuteAzevedo\Documents\ISEP\DevopsProject\devops-19-20-b-1191747\ca3
    git commit -a -m “criar pasta”
    git push

## Criar tag
    git tag -a ca3-part1 -m"FIX#19-tag ca3-parte1 "
    git push origin ca2-part2
