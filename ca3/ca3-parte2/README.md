# CA3 Parte 2

Este projeto tem por objetivo configurar 2 máquinas virtuais em Vagrant, para o trabalho realizado do Spring Basic Tutorial (ca2-parte 2). Uma das máquinas executa a parte Web dentro do Tomcat8 e a outra a base de dados H2.

## 1. Vagrant

## 1.1. Instalar Vagrant

Fazer download do Vagrant em https://www.vagrantup.com/downloads.html

No meu caso escolher a versão para windowa 64-bit. Depois de instalado, na linha de comando, se fizer *vagrant -v* consigo validar a sua instalação

### 1.2. Criar as máquinas

Ciar uma pasta e copiar o Vagranfile do Readme para esta pasta. Neste momento, o projeto implementado é o que se encontra no VagrantFile. Com:

     Vagrant up
   
São criadas 2 VM no Virtualbox, uma chamada web outra db. Com os seguintes links a funcionar para aceder a cada uma delas:

     http://localhost:8080/basic-0.0.1-SNAPSHOT/
     http://192.168.33.10:8080/basic-0.0.1-SNAPSHOT/
	 
Para aceder à consola do H2 usar os seguintes urls
	
    http://localhost:8080/basic-0.0.1-SNAPSHOT/h2-console
    http://192.168.33.10:8080/basic-0.0.1-SNAPSHOT/h2-console
	
No vagrantfile tem o url para conetar ao H2

    To connect to H2 use: jdbc:h2:tcp://192.168.33.11:9092/./jpadb
	
Fazer “test connection” para validar se está a funcionar e “connect” para aceder

### 1.3. Interpretação do VagrantFile

O VagrantFile configura as máquinas virtuais e usa a linguagem ruby. As linhas a cinzendo são apenas linhas comentadas (#)

No início, do VagrantFile estão configurações para ambas as VM. Tem depois uma “área” específica para a VM Web e outra para a de VM de BaseDados.  A configuração da VM de base dados deve ser realizada antes da Web, dado que esta última ao arrancar vai consultar a base de dados e esta deve já estar instalada.
Interpreteação de algumas linhas:

* Tudo que se encontra entre estas 2 linhas são as configurações da Vagrant, que usa como variável config

         Vagrant.configure("2") do |config|
      
          ….
       
         end
	
* A variável config tem como propriedade a VM, que por sua vez tem como propriedade a box, que está definida como Ubunto-xinial. Por omissão, cria VM com base do ubunto-xinial
   
         config.vm.box = "envimation/ubuntu-xenial"
	
Esta configuração repete-se na máquina Web e de base de dados. 

* Atribui o nomes a cada uma das máquina:

         db.vm.hostname = "db"
       
         web.vm.hostname = "web"

* Ao criar cada uma das VM são criadas novas variáveis:

	     db - config.vm.define "db" do |db|
       
     
	     web - config.vm.define "web" do |web|
	
* Em ambas as máquinas está configurado um ip “privado” para haver comunicação entre o host e as VM:

         Web: web.vm.network "private_network", ip: "192.168.33.10"
     
   
        BD: db.vm.network "private_network", ip: "192.168.33.11"


* Para aceder ao topcat, o “localhost8080” na VM corresponde “localhost8080” no host:

          web.vm.network "forwarded_port", guest: 8080, host: 8080

Para aceder ao H2

    db.vm.network "forwarded_port", guest: 8082, host: 8082
    db.vm.network "forwarded_port", guest: 9092, host: 9092

* Para instalar e configurar a VM, o config chama o provision e vai executar tudo que estiver entre <<-SHELL …..SHELL.

As seguintes configurações são implementadas nas 2 VM:

    # This provision is common for both VMs
    config.vm.provision "shell", inline: <<-SHELL
    sudo apt-get update -y
    sudo apt-get install iputils-ping -y
    sudo apt-get install -y avahi-daemon libnss-mdns
    sudo apt-get install -y unzip
    sudo apt-get install openjdk-8-jdk-headless -y
    # ifconfig
     SHELL
	 
para VM Web 

     web.vm.provision "shell", inline: <<-SHELL
      sudo apt-get install git -y
      sudo apsudo ln -s /usr/bin/nodejs /usr/bin/node
      sudot-get install nodejs -y
      sudo apt-get install npm -y
       apt install tomcat8 -y
      sudo apt install tomcat8-admin -y
      # If you want to access Tomcat admin web page do the following:
      # Edit /etc/tomcat8/tomcat-users.xml
      # uncomment tomcat-users and add manager-gui to tomcat user

      # Change the following command to clone your own repository!
      git clone https://atb@bitbucket.org/atb/tut-basic-gradle.git
      cd tut-basic-gradle
      ./gradlew clean build
      # To deploy the war file to tomcat8 do the following command:
      sudo cp build/libs/basic-0.0.1-SNAPSHOT.war /var/lib/tomcat8/webapps
    SHELL
	
e para a VM bd

    We need to download H2
    db.vm.provision "shell", inline: <<-SHELL
      wget https://repo1.maven.org/maven2/com/h2database/h2/1.4.200/h2-1.4.200.jar
    SHELL

    # The following provision shell will run ALWAYS so that we can execute the H2 server process
    # This could be done in a different way, for instance, setiing H2 as as service, like in the following link:
    # How to setup java as a service in ubuntu: http://www.jcgonzalez.com/ubuntu-16-java-service-wrapper-example
    #
    # To connect to H2 use: jdbc:h2:tcp://192.168.33.11:9092/./jpadb
    db.vm.provision "shell", :run => 'always', inline: <<-SHELL
      java -cp ./h2*.jar org.h2.tools.Server -web -webAllowOthers -tcp -tcpAllowOthers -ifNotExists > ~/out.txt &
    SHELL
	
Nota: os “-y” corresponde a “yes” caso a instalação pergunte alguma questão e permita proseguir a mesma

* Para a VM Web é instalada a seguinte memória RAM
     # We set more ram memmory for this VM
    web.vm.provider "virtualbox" do |v|
      v.memory = 1024
     end

## 2. Alterações do spring application 
 
 * Copiei o projeto da pasta ca2-part2, para uma nova pasta no ca3-parte2 
 
* Realizei as alterações no IntelliJ mencionadas no ponto 5 do trabalho

* Alterei a 2 últimas linhas do VagrantFile para indicar o meu projeto

          # Change the following command to clone your own repository!
          git clone https://1191747@bitbucket.org/1191747/devops-19-20-b-1191747.git
          cd devops-19-20-b-1191747/ca3_part2/demo
	  
* Tive alguns problemas com o projeto. Alterei a estrutura do src, apaguei o build.xml e realizei alterações nos testes, dado que não copilavam

* Na linha de comando instalei o gradle

* Na linha de comando, na do VagrantFile

   cd c:\Users\AnaRuteAzevedo\Documents\ISEP\DevopsProject\vagrant-ca3-part2
    Vagrant up
	Vagrant reload
    Vagrant ssh web
   cd devops-19-20-b-1191747/ca3/ca3-parte2$ cd demo
    git pull
    gradle clean build
	
* Para aceder ao browser utilizar os links mencionado no ponto 1.2.

## 3. Criar Tag

Depois de criar a issue no bitbucket, na linha de comando, criar a Tag

      git tag -a ca3-parte2 -m"FIX#19 "
      git push origin ca3-part2

## 4. Apresentação de uma alternativa ao Virtualbox

A Oracle - VirtualBox e a VMware são líderes de soluções de virtualização. Ambas as plataformas são rápidas, confiáveis e incluem uma variedade de recursos. A oracle fornece a VirtualBox como um hypervisior, enquanto Vmware fornece vários produtos para a execução de VM. 

A escolha entre VirtualBoxe Vmware dependem do projetos, das suas necessidades e especificidades.

VirtualBox
_________

* A VirtualBox foi lançada em 2007 e em 2008 foi adquirida pela Oracle

* Oferece suporte à virtualização de hardware e software

* solução mutiplataforma, que pode ser instalada no Linux, Windows, Solaris, mcOs e FreeBSD. Suporta um número maior sw sistemas operacionais host

* Apresenta algumas limitações para suporte gráfico 3D, sendo necessário a sua configuração manual e com limitação de memória

* Suporta diversos formatos de discos virtuais: VDI,VMDK, VHD, HDD

* Fornece diversos modos de rede: NAT, rede NAT, adaptador em ponte, rede interna, adaptador host

* Não fornece recursos de cluster

VMware
_______

* Vmware foi lançado em 1999, sendo neste momento uma família de vários aplicativos e ferramentas de virtualização: Vmawre Workstation, Player,  Fusion e  ESX

* Oferece suporte à virtualização apenas de hardware

* O VMware Worksration e Player podem ser instalado no Linux e no Windows e o Vmware Fusion em macOS

* No suporte gráfico 3D fornece melhor solução

* Suporta apenas VMDK como disco virtuais

* Fornece menos modos de rede:  NAT, adaptador host e ponte. No Vmare Workstation e Fusion incluem um editor de rede virtual.

* Tem capacidade de implementar 2 tipos de clusters usando servodores ESXi e vCenter no vSphere

Ambos
_________
* São ambos produtos de software livre de código aberto.

* Suportam a conexão de dispositivos USB, isto é, conetar um dispositivo USB à máquina host e coneta-la à VM, especialmente útil para ligar câmeras, adaptadores wi-fi, discos rígidos, impressoras, etc

* Fáceis de usar, com uma curva de aprendizagem curta, apesar da complexidade de conceitos e recursos disponíveis

Nota: existem alguns aspectos que difernciam os 2 sistemas, mas com conceitos mais complexos que não abordei aqui.

Implementação
-------------------

* Tentei implementar VMware Worktation por se assemelhar  ao VirtualBox

* Começei por fazer dowload do VMWare workstation no site: https://www.vmware.com/products/workstation-pro/workstation-pro-evaluation.html

* Para utilizar o Vagrant com o Vmware, a única solução disponível seria a compra de uma licença, o que impediu que implementasse a solução alternativa como um todo

* No VirtualBox fiz a exportação das MV: File/<selecionar a máquina em causa>/export

* Previamente tinha criado uma pasta onde guardei os 2 ficheiros

* As 2 máquinas são guardados com o formato “.ova”

* No Vmware importar as VM: “create newVirtual Machine” e “installer disc image file”

* Neste momento, é possível com 2 clicks abrir as VM, no entanto seria necessário ter login e password para po-las correr. Como foram criadas com Vagrant não é possível .

* Caso tivesse comprado a licença de Vagran para Vmware era necessário instalar:

    Vagrant plugin licence vagrant-vmware-desktop  <nº da licença>
    
    Vagrant plugin install vagrant-vmware-desktop

