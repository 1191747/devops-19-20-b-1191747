/*
 * Copyright 2015 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.basic;

import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * @author Greg Turnquist
 */
// tag::code[]
@Entity // <1>
public class Employee {

	private @Id @GeneratedValue Long id; // <2>
	private String firstName;
	private String lastName;
	private String description;
	private String jobTitle;
	private String email;


	public Employee() {}

	public Employee(String firstName, String lastName, String description, String jobTitle, String email) {
		this.lastName = lastName;
		this.description = description;
		this.jobTitle=jobTitle;
		this.email=email;

		setFirstName(firstName);
		setLastName(lastName);
		setDescription(description);
		setJobTitle(jobTitle);
		setEmail(email);
	}


	public void setFirstName(String firstName) {
		if (firstName == null){
			throw new NullPointerException("FirstName can not be null");}
		if (firstName.isEmpty() && firstName.trim().isEmpty()){
			throw new IllegalArgumentException("FirstName is invalid");}
				this.firstName = firstName;
	}

	public void setLastName(String lastName) {
		if (lastName == null){
			throw new NullPointerException("LastName can not be null");}
		if (lastName.isEmpty() && lastName.trim().isEmpty()){
			throw new IllegalArgumentException("LastName is invalid");}
		this.lastName = lastName;
	}

	public void setDescription(String description) {
		if (description == null){
			throw new NullPointerException("Description can not be null");}
		if (description.isEmpty() && description.trim().isEmpty()){
			throw new IllegalArgumentException("Description is invalid");}
		this.description = description;
	}

	public void setJobTitle(String jobTitle) {
		if (jobTitle == null){
			throw new NullPointerException("Job Title can not be null");}
		if (jobTitle.isEmpty() && jobTitle.trim().isEmpty()){
			throw new IllegalArgumentException("Job Title is invalid");}
		this.jobTitle = jobTitle;
	}

	public void setEmail(String email) {
		if (email == null) {
			throw new NullPointerException("e-Mail can not be null");
		}
		if (email.isEmpty() && email.trim().isEmpty()) {
			throw new IllegalArgumentException("e-Mail is invalid");
		}
		if (!email.contains("@")) {
			throw new IllegalArgumentException("e-mail is invalid without @");
		}

		Pattern regexPattern = Pattern.compile("^[(a-zA-Z-0-9-\\_\\+\\.)]+@[(a-z-A-z)]+\\.[(a-zA-z)]{2,3}$");
		Matcher regMatcher = regexPattern.matcher(email);
		if (!regMatcher.matches()) {
			throw new IllegalArgumentException("e-mail is invalid");
		}

		this.email = email;

	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Employee employee = (Employee) o;
		return Objects.equals(id, employee.id) &&
			Objects.equals(firstName, employee.firstName) &&
			Objects.equals(lastName, employee.lastName) &&
			Objects.equals(description, employee.description)&&
				Objects.equals(jobTitle, employee.jobTitle)&&
				Objects.equals(email, employee.email);

	}

	@Override
	public int hashCode() {

		return Objects.hash(id, firstName, lastName, description, jobTitle, email);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public String getDescription() {
		return description;
	}

	public String getJobTitle() {
		return jobTitle;
	}

	public String getEmail() {
		return email;
	}

	@Override
	public String toString() {
		return "Employee{" +
				"id=" + id +
				", firstName='" + firstName + '\'' +
				", lastName='" + lastName + '\'' +
				", description='" + description + '\'' +
				", jobTitle='" + jobTitle + '\'' +
				", email='" + email + '\'' +
				'}';
	}
}
// end::code[]
