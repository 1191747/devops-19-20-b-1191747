package com.basic;


import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertThrows;

class EmployeeTest {
    Employee employee;

    @BeforeEach

    @DisplayName("Validate emplotee - Happy Path")
    @Test

    public void validateEmployeeHappyPath() {

        //Arrange
        employee = new Employee("Ana", "Silva", "description", "Director", "ana@gmail.com");
        Employee expected =new Employee("Ana", "Silva", "description", "Director", "ana@gmail.com");
        //Act
        Boolean result = employee.equals(expected);
        // Assert
        assertTrue(result);
    }

    @DisplayName ("Validate employee - firstName - null")
    @Test

    void validateFirstNameNull() {

        //Arrage//Act//Asser
        assertThrows(NullPointerException.class, () -> {
            employee = new Employee(null, "Silva", "description", "Director", "ana@gmail.com");
        });
    }

    @DisplayName ("Validate employee - firstName - empty")
    @Test

    void validateFirstEmpty() {

        //Arrage//Act//Asser
        assertThrows(IllegalArgumentException.class, () -> {
            employee = new Employee("", "Silva", "description", "Director", "ana@gmail.com");
        });
    }

    @DisplayName ("Validate emplotee - lastName - null")
    @Test

    void validateLastNameNull() {

        //Arrage//Act//Asser
        assertThrows(NullPointerException.class, () -> {
            employee = new Employee("Ana", null, "description", "Director", "ana@gmail.com");
        });
    }

    @DisplayName ("Validate emplotee - lastName - empty")
    @Test

    void validateLastNameEmpty() {

        //Arrage//Act//Asser
        assertThrows(IllegalArgumentException.class, () -> {
            employee = new Employee("Ana", "", "description", "Director", "ana@gmail.com");
        });
    }

    @DisplayName ("Validate emplotee - Description - null")
    @Test

    void validateLastDescriptionNull() {

        //Arrage//Act//Asser
        assertThrows(NullPointerException.class, () -> {
            employee = new Employee("Ana", "Silva", null, "Director", "ana@gmail.com");
        });
    }

    @DisplayName ("Validate emplotee - Description - empty")
    @Test

    void validateDescriptionEmpety() {

        //Arrage//Act//Asser
        assertThrows(IllegalArgumentException.class, () -> {
            employee = new Employee("Ana", "Silva", "", "Director", "ana@gmail.com");
        });
    }

        @DisplayName ("Validate emplotee - jobTitle - null")
        @Test

        void validateJobTitleNull() {

            //Arrage//Act//Asser
            assertThrows(NullPointerException.class, () -> {
                employee = new Employee("Ana", "Silva", "description", null, "ana@gmail.com");
            });
        }

        @DisplayName ("Validate emplotee - jobTitle - empty")
        @Test

        void validateJobTitleEmpty() {

            //Arrage//Act//Asser
            assertThrows(IllegalArgumentException.class, () -> {
                employee = new Employee("Ana", "Silva", "description", "", "ana@gmail.com");
            });
        }

    @DisplayName ("Validate emplotee - email - null")
    @Test

    void validateEMailNull() {

        //Arrage//Act//Asser
        assertThrows(NullPointerException.class, () -> {
            employee = new Employee("Ana", "Silva", "description", "Director", null);
        });
    }

    @DisplayName ("Validate emplotee - email - empty")
    @Test

    void validateEmailEmpty() {

        //Arrage//Act//Asser
        assertThrows(IllegalArgumentException.class, () -> {
            employee = new Employee("Ana", "Silva", "description", "Director", "");
        });
    }


    @DisplayName ("Validate emplotee - email - without @")
    @Test

    void validateEmailWithoutA() {

        //Arrage//Act//Asser
        assertThrows(IllegalArgumentException.class, () -> {
            employee = new Employee("Ana", "Silva", "description", "Director", "anagmail.com");
        });
    }
}