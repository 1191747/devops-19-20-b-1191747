# CA4

Este projeto tem por objetivo, executar o trabalho realizado do Spring Basic Tutorial (ca2-parte 2) num container, configurando o Docker.

## 1. Instalação

* No site  da Docker https://www.docker.com/products/docker-desktop, fazer download do Docker, no meu caso da versão Windows

* Tendo o Hyper -v no Windows não tive problemas na sua instalação

* Colocando o Docker a correr, na linha de comando posso ver as suas características com: docker info

* Para obter ajuda para saber os comando existentes: docker –help

* Fiz download dos ficheiros disponibilizados no ReadMe em: https://bitbucket.org/atb/docker-compose-spring-tut-demo/.  

* Estes ficheiros incluem: docker-compose, uma pasta Web, db e data

### 1.1. Pasta Web  

* Na pasta web encontra-se o DockerFile para configurar o container web, onde correr o TomCat e a aplicação do Sping Tuturial

* Esta pasta web tem que ter o mesmo nome do serviço mencionado no docker-compose

* Existe já uma imagem do TomCat, que o vai correr: “FROM tomcat”

* Existem diversos “RUN” para instalar tudo que é necessário

* Foi necessário remover os node_modules, que se encontravam do projeto e que não permitiam fazer docker-compose build

           RUN rm -rf devops-19-20-b-1191747/ca3/ca3-parte2/demo/node_modules
		   
* Alterei o fichero clone para o meu projetos:

      git clone https://1191747@bitbucket.org/1191747/devops-19-20-b-1191747/ca3_part2/demo
      
      WORKDIR /tmp/build/devops-19-20-b-1191747/ca3/ca3-parte2/demo

* Para fazer o build do Gradle: RUN ./gradlew clean build

* Usa o seguinte comando para copiar o ficheiro WAR para a pasta desta imagem que tem a aplicação TomCat

        RUN cp build/libs/demo-0.0.1-SNAPSHOT.war /usr/local/tomcat/webapps/
		
* Utiliza a port 8080: EXPOSE 8080

### 1.2. Pasta db

* Na pasta db encontra-se o DockerFile para configurar o container de base de dados, onde corre  H2

* Esta pasta db tem que ter o mesmo nome do serviço mencionado no docker-compose

* Com base no Ubuntu (FROM ubuntu), mas no final é necessário mencionar o que é para correr: CMD java -cp ./h2-1.4.200.jar org.h2.tools.Server -web -webAllowOthers -tcp -tcpAllowOthers -ifNotExists

* Existem diversos “RUN” para as app e instalar nomeadamente o java, unzip e wget(instalar H2)

* Expões as ports: 8082 e 9092

### 1.3. Pasta Data

* Pasta  para onde vão ser mapeados os Volumes

* Este comando é dado no docker-compose 

### 1.4. Docker-Compose

* Cada Container é para apenas um único serviço, sendo necessário um Docker Compose 

* Dentro do Docker Compose existem 2 serviços, ou seja, 2 contentores um chamado web outro bd

* Como é realizado um “build” é necessário existir um DockerFile com o nome web e outro db, para construir o Docker Compose

* O Docker compose configura as ports de acesso e o IP de cada serviço. 

* Como o Container Web utiliza o Container da Base de dados para arrancar é necessário que este último arranque primeiros. Esta dependência é configurada com:

	Depends_on:
	-“db”
	
* Mapeamento de Volumes da pasta do meu computador (./data), que grava para o contentor (/usr/src/data)

* É criada uma rede local para os 2 contentor: 192.168.33.0/24, para a web 192.168.33.10 e para db 192.168.33.11

* Foi necessário colocar um √ no Docker/Settings/Resources/File Sharing/Select the local drives you want to be available to your containers/ C

## 2. Depois de tudo instalado

* Abrir o terminal de comando, onde se encontram todos os documentos e:

    docker-compose build
      
    docker-compose up
	
* Usar o link para aceder à aplicação web:  http://localhost:8080/basic-0.0.1.SNAPSHOT/

* Para aceder à base de dados no: http://localhost:8082/ e com o seguinte user jdbc:h2:tcp://192.168.33.11:9092/./jpadb

* Para desligar os 2 serviços :  

     Docker-compose down

## 3. Docker Hub

* Acedi ao site do Docker Hub (https://hub.docker.com) e realizei o registo

* Criei no site um repositório com o nome: isep_devops_ca4

* Na linha de comando

           docker login (coloquei user e password)
           
* Para validar o nome das imagens

         docker ps -a
         
* Tagname: ca4_web e ca4_db

         docker tag ca4_web anaruteazevedo/isep_devops_ca4:ca4_web
           
         docker push anaruteazevedo/isep_devops_ca4:ca4_web
           
         docker tag ca4_db anaruteazevedo/isep_devops_ca4:ca4_db
        
         docker push anaruteazevedo/isep_devops_ca4:ca4_db

## 4.  Volume

Os volumes são um mecanismo  para a persistência de dados gerados e usados pelos container do Docker. Os volumes são geridos pelos Containers.

	Docker-compose start
    
    Docker-compose exec db /bin/bash – executa dentro do docker db o bin/bash (shell de Linux)
    
    root@b429e1665d41:/usr/src/app#   - em que b429e1665d41 é o idenificador do contentar e está dentro da pasta :/usr/src/app
    
    ps -ax   - mostra todos os processos a correr e podemos ver que o java está a correr
    
    ls -al  - mostra os ficheiros e inclui o ficheiro da dase de dados jpadb.mv.db
    
    cp ./jpadb.mv.db /usr/src/data – copias a base de dados para a pasta data no meu computador
   

Caso o conainer seja destruído, os dados encontram-se armazenados na pasta data.
            
## 6. Criar Tag

Depois de criar a issue no bitbucket, na linha de comando, criar a Tag 

      git tag -a ca4 -m"FIX#28 " 
      
      git push origin ca4

## 7. Kubernetes

* Lançado em junho de 2014, originalmente projetado pela Google e atualmente mantido pela Cloud Native Computing Fondation
	 
* É um sistema em open-source utilizado para automatizar a implantação, o dimensionamento e a gestão de aplicativos em containers.

* O Kubernetes possibilita:

      - Orquestrar containers em vários hosts
   
      - Aproveitar melhor o hardware para maximizar os recursos necessários na execução das aplicações corporativas
   
      - Controlar e automatizar as implantações e atualizações de aplicações
    
      - Montar e adicionar armazenamento para executar aplicações com monitoração de estado.

      - Escalar rapidamente as aplicações em containers e recursos relacionados.

      - Gerenciar serviços de forma declarativa, garantindo que as aplicações sejam executadas sempre da mesma maneira como foram implantadas.

      - Verificar a integridade e autorrecuperação das aplicações com posicionamento, reinício, replicação e escalonamento automáticos.

* Alguns conceitos do Kubernetes

      - **Master**: a máquina que controla os nós do Kubernetes. É nela que todas as atribuições de tarefas se originam.

      - **Nó**: são máquinas que realizam as tarefas solicitadas e atribuídas. A máquina mestre do Kubernetes controla os nós.
 
      - **Pod**: um grupo de um ou mais containers implantados em um único nó. Todos os containers em um pod compartilham o mesmo endereço IP, IPC, nome do host e outros recursos. Os pods separam a rede e o armazenamento do container subjacente. Isso facilita a movimentação dos containers pelo cluster.
 
      - **Controlador de replicações**: controla quantas cópias idênticas de um pod devem ser executadas em um determinado local do cluster.

      - **Serviço**: desacopla as definições de trabalho dos pods. Os proxies de serviço do Kubernetes automaticamente levam as solicitações de serviço para o pod correto, independentemente do local do pod no cluster ou se foi substituído.

      - **Kubelet**: um serviço executado nos nós que lê os manifestos do container e garante que os containers definidos foram iniciados e estão em execução.

      - **kubectl**: a ferramenta de configuração da linha de comando do Kubernetes.
   
* O Minikube é uma ferramenta que facilita a execução local de Kubernetes, dado que executa um único nó. Os seus comandos são em tudo iguais aos do Kubernetes.

* Instalação:

      - Dowload kubectl

      - Transferi o ficheiro exe para uma pasta dentro do PC, que chamei Kubernets

      - Nas enviroment variable/path, editar e acrescentar: C:\Users\AnaRuteAzevedo\Documents\ISEP\DeveOps\Kubernetes

      - na linha de comando para obter informação digitalizar: kubectl

      - Fazer down Minikube e colocar o ficheiro exe, na mesma pasta para também este estar configurado nas enviroment variable


