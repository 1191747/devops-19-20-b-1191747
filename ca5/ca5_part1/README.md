# CA5 Parte 1

Este projeto tem por objetivo implementar o Jenkins para o projeto já realizado Gradle basic demo (ca2-part1)

## 1. Instalar Jenkins

* Aceder ao site: https://www.jenkins.io/download/

* Fazer download da versão Long-term Support (LTS) “Generic Java package (.war)

* Criar uma pasta para colocar o ficheiro gerado

* Executar na linha de comando, os seguintes comando, sendo gerado um ficheiro com o password em:\Users\AnaRuteAzevedo\.jenkins\secrets\initialAdminPassword:

       cd C:\Users\AnaRuteAzevedo\Documents\ISEP\DeveOps\Jenkins
	   
       java -jar jenkins.war

* Abrir o browser em  http://localhost:8080 e colocar a password do ficheiro

* Instalar sugestão de plugins

## 2. Configurar credenciais no Jenkins:

 No menu do Jenkins selecionar Credentials/ global e adicionar novas credênciais, optando por:
 
      - Kind: Username with password 
	  
     - scope Global (Jenkins, nodes,items, allchild items, etc)
	 
     - definindo ID: 1191747
	 
## 3. Criar o Jenkinfile

* Criar num ficheiro texto:

   		 pipeline{
		 
			agent any
			
			stage(‘Checkout’)
			
				step{
				
					echo	‘Checking out…’
					
					git credentialsId: '1191747', url: 'https://bitbucket.org/1191747/devops-19-20-b-1191747'
					
				}
				
			}
			
			stage(‘Assemble’){
			
				steps{
				
					echo ‘Assembling…’
					
					bat 'cd ca2/ca2_part1 & gradlew clean assemble'
					
					}
					
				}
				
			stage(‘Test’){
			
				step{
				
					echo ‘Testing…’
					
					bat 'cd ca2/ca2_part1 & gradlew test'
					
					}
					
				}
				
			Stage(‘Archive’){
			
				echo ’Archoving…’
				
				archiveArtifacts 'ca2/ca2_part1/build/distributions/*'
				
				}
				
			}
			
		}
		
  	}

* O ficheiro Jenkinsfile é um plugin, que define os pipelines, ou seja, o que vai ser executado em cada stages. Neste trabalho foram solicitados 4  stages:

     -  Checkout – o ID definidas nas credênciais do Jenkins para o Bitbucket são aqui utilizadas, garantindo o acesso ao repositório através do seu URL.  Vai fazer o check-out do código do repositório do  Bitbucket;
	 
     - Assemble – compila e produz os ficheiros de arquivo
	  
 	  - Test – executa os testes e publica-os no Jenkins os seus resultados

	  - Archive – arquiva no Jenkins os ficheiros gerados no stage Assemble

* Gravei o Jankinfile na mesma pasta onde se encontra o projecto ca2-part1

## 4. Atualizar repositório Bitbucket

Criar issue 29 para fazer atualizar o repositório com o ficheiro Jankinfile

      cd  C:\Users\AnaRuteAzevedo\Documents\ISEP\DevopsProject\devops-19-20-b-1191747\ca2\ca2_part1
	  
      git add .
	  
      git commit -a -m  “Fix#29”
	  
      git push

## 5. Configurar o Job

* Aceder no Jenkins ao menu configure
* O Job é de tipo pipeline e seleccionar:

       - Definition: Pipeline script from SCM
	   
       - SCM: GIT
	   
	  - repository URL: https://bitbucket.org/1191747/devops-19-20-b-1191747
	  
	  - Credenciais: anaazevedo/******** 
	  
	  - Branch Specifier: +/master
	  
      - Script Path: ca2/ca2_part1/jenkinsfile
	  
      - Apply e save

* No menu seleccionar “Build now”

* Na Console Output posso validar como está ser feito o build

## 6. Criar Tag ca5-part1

Depois de criar a issue no bitbucket, na linha de comando, criar a Tag

    git tag -a ca5_part1 -m"FIX#30 "

    git push origin ca5_part1
