# Ca5_part2 

O objetivo deste trabalho é criar um pipeline no Jankins, para o projeto em gradle do tutorial spring boot application (ca2-part2)

## 1. Criar o Jenkinfile

Neste trabalho foram solicitados 6 stages:

* Checkout – o ID definidas nas credênciais do Jenkins para o Bitbucket são aqui utilizadas, garantindo o acesso ao repositório através do seu URL. Vai fazer o check-out do código do repositório do Bitbucket;

* Assemble – compila e produz os ficheiros de arquivo

* Test – executa os testes e publica-os no Jenkins os seus resultados

* Javadoc – gera o javadoc e publica-o

* Archive – arquiva no Jenkins os ficheiros gerados no stage Assemble

* Publish Image - gera uma imagem do docker com o Tomcat e o ficheiro war e publique-o no Docker Hub.

     pipeline {
     
      agent any
      
        stages {
        
         stage('Checkout') {
         
            steps {
            
                echo 'Checking out...'
                
                git credentialsId: '1191747', url: 'https://1191747@bitbucket.org/1191747/devops-19-20-b-1191747/'
                
            }
            
        }
        
        stage('Assemble') {
        
            steps {
            
                echo 'Assembling...'
                
                dir('ca5/ca5_part2'){
                
                    bat 'gradle clean assemble'
                    
                }
            }
            
        }
        
        stage('Javadoc') {
        
            steps {
            
                echo 'Generating Javadoc...'
                
                dir('ca5/ca5_part2'){
                
                
                    bat 'gradle javadoc'
                }
                
                publishHTML (target : [allowMissing: false,
                
                alwaysLinkToLastBuild: true,
                
                keepAll: true,
                
                reportDir: 'ca5/ca5_part2/build/docs/javadoc',
                
                reportFiles: 'index.html',
                
                reportName: 'My Reports',
                
                reportTitles: 'The Report'])
                
            }
            
        }
        
        stage('Archive') {
        
            steps {
            
                echo 'Archiving...'
                
                archiveArtifacts 'ca5/ca5_part2/build/libs/*'
                
            }
            
        }
        
        stage('Pubish Image') {
        
            steps {
            
                echo 'Publishing docker image...'
                
                script{
                
                    docker.build("anaruteazevedo/ca5-parte2:${env.BUILD_ID}", "ca5/ca5_part2").push()
                    
                }
                
            }
        }
        }
    }

## 2.  Alterar o repositório

* O copiei o ficheiro dockerfile web para a pasta do projeto ca5_part2

* Criar o ficheiro com o Jenkinfile mencionado no ponto 1

* Copiei o projecto ca2-part2 para ca5_part5

## 3. Configurar o Job

* Para aceder ao Jenkins, na consola

     cd C:\Users\AnaRuteAzevedo\Documents\ISEP\DeveOps\jenkinfile
	 
	 java -jar jenkins.war
	 
* No browser aceder http://localhost:8080 com o user e password criados no ca5-part1

* Aceder no Jenkins ao menu configure

* Criar o Job:
	
	- Definition: Pipeline script from SCM
	
	- SCM: GIT
	
 	- repository URL: https://bitbucket.org/1191747/devops-19-20-b-1191747

	- credenciais: anaazevedo/**
	
	- Branch Specifier: +/master
	
	- Script Path: ca5/ca5_part5/jenkinsfile
	
	- Apply e save
	
* No menu selecionar “Build now”

* Na Console Output posso validar como está ser feito o build

## 4. Docker Hub

* No Jenkins, nas criei as credências para aceder ao DockerHub, no entanto como no meu caso é público não era necessário

* Usei o DockerHub que criei no trabalho Ca4, cuja nome é anaruteazevedo

* com a stage Pubish Image foi criado novo repositório com o nome ca5_part2



## 5. Criar Tag ca5-part2

* Depois de criar a issue no bitbucket, na linha de comando, criar a Tag

     git tag -a ca5_part2 -m"FIX#31 "

     git push origin ca5_part2

## 5. Azure Devops vs Jenkins

Jenkins
--------

* Jenkins é provavelmente a ferramenta número 1 de integração contínua (e entrega contínua) para desenvolvedores Java  

* Código aberto, gratuito e fácil de instalar

* Disponível para todas as plataformas e sistemas operacionais (Mac, Windows, Linux)

* Suportada por uma ampla variedade de plugins, mas que por vezes as suas dependências são complicadas

* Interface um pouco desatualizada e limitada

* Para obter estatísticas e métricas é necessário instalar alguns plugins

* Os piplines com script tem que ser programados no Groovy

* Não tem interface YAML para os piplines

* Fluxo de trabalho com script muito flexível, incluindo fluxo de trabalho complexos

* A rastreabilidade das alterações no pipeline é rastreada apenas se o pipeline estiver no Git. A rastreabilidade de um build e release deve ser programada no pipeline

* Suporte a credenciais e arquivos secretos

* Seguro

Azure DevOps
---------------

* Anteriormente conhecido por Visual Studio Team Service

* Fácil de usar

* Permite encapsular uma sequência de tarefas, já definidas num pipeline numa única tarefa reutilizável, como qualquer outra tarefa.

* O fluxo de trabalho é limitado e direto, o que dificulta o desenvolvimento de fluxos de trabalho complexos

* Pode-se definir pipelines usando um classico editor e o YAML. A construção por meio de uma GUI é muito mais fácil e rápida

* Boa interface com o utilizador, fornece no final de cada execução, parâmetros como taxa e duração da execução

* A integração com não Microsoft é difícil

* Nas piplines o  fluxo de trabalho é direto (não é possível definir construções if-else ou switch-case), o que  dificulta o desenvolvimento de fluxos de trabalho complexos.

* As mudanças no pipeline são registradas (toda mudança é uma confirmação). Todas as etapas de um build e release são rastreáveis prontas para uso

* Suporte a credenciais e arquivos secretos
 
* Seguro
