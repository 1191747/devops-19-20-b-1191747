package com.greglturnquist.payroll;


import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

class EmployeeTest {

    @org.junit.jupiter.api.Test
    void EmployeeConstructor() {

        //Arrange,Act,Assert
        assertThrows(IllegalArgumentException.class, () -> {
            new Employee("Frodo", "Baggins", "ring bearer", null);
        });
    }

    @org.junit.jupiter.api.Test
    void EmployeeValidatieInvalidEmail() {
        //Arrange,Act,Assert
        assertThrows(IllegalArgumentException.class, () -> {
            new Employee("Frodo", "Baggins", "ring bearer", "alexatgmail.com");
        });
    }

    @org.junit.jupiter.api.Test
    void EmployeeValidateEmailHappyPath() {
        //Arrange,Act
        new Employee("Frodo", "Baggins", "ring bearer", "alex@gmail.com");
        //Assert
        assertTrue(true);
    }
}